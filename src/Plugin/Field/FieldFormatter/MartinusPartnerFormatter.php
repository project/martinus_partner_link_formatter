<?php

declare(strict_types=1);

namespace Drupal\martinus_partner_link_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'Martinus Partner' formatter.
 *
 * @FieldFormatter(
 *   id = "martinus_partner_link_formatter_martinus_partner",
 *   label = @Translation("Martinus Partner"),
 *   field_types = {"link"},
 * )
 */
final class MartinusPartnerFormatter extends LinkFormatter {
  const HOSTS = [
    'sk' => 'Martinus.sk',
    'cz' => 'Martinus.cz',
  ];
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $setting = [
      'partner_id' => '',
      'hosts' => [],
    ];
    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements['partner_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partner ID'),
      '#default_value' => $this->getSetting('partner_id'),
    ];
    $elements['hosts'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Which host does this apply to?'),
      '#options' => self::HOSTS,
      '#default_value' => $this->getSetting('hosts'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      $this->t('Partner ID: @id<br>Applies for domains: @hosts', [
        '@id' => $this->getSetting('partner_id'),
        '@hosts' => implode(', ', $this->getApplicableHosts()),
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = parent::viewElements($items, $langcode);
    $partner_id = $this->getSetting('partner_id');
    $hosts = ($this->getApplicableHosts());
    $pattern = '/(www)?\.?martinus\.(' . implode('|', $hosts) . ')/m';
    $args = [
      'z' => $partner_id,
      'utm_source' => 'z=' . $partner_id,
      'utm_medium' => 'url',
      'utm_campaign' => 'partner'
    ];
    foreach ($element as &$item) {
      /* @var \Drupal\Core\Url $url */
      $url = $item['#url'];
      if ($url->isExternal() && $hosts) {
        $url_parts = parse_url($url->getUri());
        if (preg_match($pattern, $url_parts['host'])) {
          $url->setOptions(['query' => $args]);
        }
      }
    }
    return $element;
  }

  private function getApplicableHosts() {
    $filtered = [];
    $hosts = $this->getSetting('hosts');
    foreach ($hosts as $key => $host) {
      if ($host !== 0) {
        $filtered[] = $key;
      }
    }
    return $filtered;
  }

}
